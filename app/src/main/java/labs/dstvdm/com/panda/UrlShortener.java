package labs.dstvdm.com.panda;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class shortens urls and also expands those short urls
 * to their original url.
 * Currently this class only supports google url shortener
 * <p/>
 * TODO: Rename this class since url shortening has been refactored to the
 * PwsClient.
 */
class UrlShortener {

    private static final String TAG = "UrlShortener";

    /**
     * A callback to be invoked when done lengthening or shortening a url
     */
    interface ModifiedUrlCallback {
        void onNewUrl(String newUrl);

        void onError(String oldUrl);
    }

    /**
     * Check if the given url is a short url.
     *
     * @param url The url that will be tested to see if it is short
     * @return The value that indicates if the given url is short
     */
    public static boolean isShortUrl(String url) {
        return url.startsWith("http://goo.gl/") || url.startsWith("https://goo.gl/");
    }

    /**
     * Takes any short url and converts it to the long url that is being pointed to.
     * Note: this method will work for all types of shortened urls as it inspect the
     * returned headers for the location.
     */
    public static class LengthenShortUrlTask extends AsyncTask<String, Void, String> {
        private ModifiedUrlCallback mCallback;
        private String mShortUrl;

        LengthenShortUrlTask(ModifiedUrlCallback callback) {
            mCallback = callback;
        }

        @Override
        protected String doInBackground(String[] params) {
            mShortUrl = params[0];
            try {
                URL url = new URL(mShortUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setInstanceFollowRedirects(false);
                String longUrl = httpURLConnection.getHeaderField("location");
                return (longUrl != null) ? longUrl : mShortUrl;
            } catch (MalformedURLException e) {
                Log.w(TAG, "Malformed URL: " + mShortUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                mCallback.onError(mShortUrl);
            } else {
                mCallback.onNewUrl(result);
            }
        }
    }
}
