package labs.dstvdm.com.panda;

import java.util.Comparator;
import java.util.HashMap;

/**
 * A comparator that sorts urls based on their metadata.
 * <p/>
 * This comparator needs to be initialized with a map from urls to metadata.
 * It will use them to perform the comparing.
 */
public class MetadataComparator implements Comparator<String> {
    private final HashMap<String, PwsClient.UrlMetadata> mUrlToUrlMetadata;

    public MetadataComparator(final HashMap<String, PwsClient.UrlMetadata> urlToUrlMetadata) {
        mUrlToUrlMetadata = urlToUrlMetadata;
    }

    /**
     * Compare two urls.
     */
    @Override
    public int compare(String urlA, String urlB) {
        PwsClient.UrlMetadata urlMetadataA = mUrlToUrlMetadata.get(urlA);
        PwsClient.UrlMetadata urlMetadataB = mUrlToUrlMetadata.get(urlB);

        // Return the best rank given by proxy server
        // If a url lacks metedata, the other should come first.
        if (urlMetadataA == null) {
            return 1;
        }
        if (urlMetadataB == null) {
            return -1;
        }
        int rankCompare = ((Float) urlMetadataA.rank).compareTo(urlMetadataB.rank);
        if (rankCompare != 0) {
            return rankCompare;
        }

        // If ranks are equal, compare based on title
        return urlMetadataA.title.compareTo(urlMetadataB.title);
    }
}
