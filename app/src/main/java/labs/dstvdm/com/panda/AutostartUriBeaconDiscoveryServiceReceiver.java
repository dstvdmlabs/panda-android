package labs.dstvdm.com.panda;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * This receiver starts the UriBeaconDiscoveryService
 */
public class AutostartUriBeaconDiscoveryServiceReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String preferences_key = context.getString(R.string.panda_preference_file_name);
        SharedPreferences sharedPreferences =
                context.getSharedPreferences(preferences_key, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(context.getString(R.string.user_opted_in_flag), false)) {
            Intent newIntent = new Intent(context, UriBeaconDiscoveryService.class);
            context.startService(newIntent);
        }
    }
}
