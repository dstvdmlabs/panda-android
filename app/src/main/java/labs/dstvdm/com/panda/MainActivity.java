package labs.dstvdm.com.panda;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import labs.dstvdm.com.panda.rtc.PandaRTC;
import labs.dstvdm.com.panda.trilateration.PandaMap;

/**
 * The main entry point for the app.
 */

public class MainActivity extends Activity {
    private static final int REQUEST_ENABLE_BT = 0;
    private static final String NEARBY_BEACONS_FRAGMENT_TAG = "NearbyBeaconsFragmentTag";
    public  String IMEI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IMEI = getIMEI();
        Toast.makeText(getBaseContext(), IMEI, Toast.LENGTH_LONG).show();
        // check if device is registered
        //TODO do an async call to service to see if this device is owned yet.
        /** Will update user db REST service with IMEI number to "claim" a profile
         * We will probably have to do this as a webview component though. Make it easier to manage
         * We may also want to add in the social junk here too like twitter and fb profiles
         * we should also have a look at vCards etc from AD
         */

    }

    /**
     * Ensures Bluetooth is available on the beacon and it is enabled. If not,
     * displays a dialog requesting user permission to enable Bluetooth.
     */
    private void ensureBluetoothIsEnabled() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Called when a menu item is tapped.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // If the configuration menu item was selected
            case R.id.action_config:
                showBeaconConfigFragment();
                return true;
            // If the about menu item was selected
            case R.id.action_about:
                showAboutFragment();
                return true;
            // If the demo menu item was selected
            case R.id.action_chat:
                Toast.makeText(this, "Opening chat", Toast.LENGTH_SHORT).show();
                Intent chatintent = new Intent("com.xabber.android.ui.LoadActivity");
                startActivity(chatintent);
                return true;
            // If the action bar up button was pressed
            case android.R.id.home:
                getFragmentManager().popBackStack();
                getActionBar().setDisplayHomeAsUpEnabled(false);
                return true;

            case R.id.action_videocall:
                Intent intent = new Intent(this, PandaRTC.class);
                startActivity(intent);
                return true;

            case R.id.action_maps:
                Intent mapintent = new Intent(this, PandaMap.class);
                startActivity(mapintent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (checkIfUserHasOptedIn()) {
            // The service runs when the app isn't running.
            startUriBeaconDiscoveryService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkIfUserHasOptedIn()) {
            ensureBluetoothIsEnabled();
            // The service pauses while the app is running since the app does it's own scans or
            // is configuring a UriBeacon using GATT which doesn't like to compete with scans.
            stopUriBeaconDiscoveryService();
            showNearbyBeaconsFragment(false);
        } else {
            // Show the oob activity
            Intent intent = new Intent(this, OobActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /**
     * Stop the beacon discovery service from running.
     */
    private void stopUriBeaconDiscoveryService() {
        Intent intent = new Intent(this, UriBeaconDiscoveryService.class);
        stopService(intent);
    }

    /**
     * Start up the BeaconDiscoveryService
     */
    private void startUriBeaconDiscoveryService() {
        Intent intent = new Intent(this, UriBeaconDiscoveryService.class);
        startService(intent);
    }

    public String getPhoneName() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = myDevice.getName();
        return deviceName;
    }

    public String getIMEI() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }

    /**
     * Show the fragment scanning nearby UriBeacons.
     */
    private void showNearbyBeaconsFragment(boolean isDemoMode) {
        if (!isDemoMode) {
            // Look for an instance of the nearby beacons fragment
            Fragment nearbyBeaconsFragment = getFragmentManager().findFragmentByTag(NEARBY_BEACONS_FRAGMENT_TAG);
            // If the fragment does not exist
            if (nearbyBeaconsFragment == null) {
                // Create the fragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_activity_container, NearbyBeaconsFragment.newInstance(isDemoMode), NEARBY_BEACONS_FRAGMENT_TAG)
                        .commit();
                // If the fragment does exist
            } else {
                // If the fragment is not currently visible
                if (!nearbyBeaconsFragment.isVisible()) {
                    // Assume another fragment is visible, so pop that fragment off the stack
                    getFragmentManager().popBackStack();
                }
            }
        } else {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_up_fragment, R.anim.fade_out_fragment, R.anim.fade_in_activity, R.anim.fade_out_fragment)
                    .replace(R.id.main_activity_container, NearbyBeaconsFragment.newInstance(isDemoMode))
                    .addToBackStack(null)
                    .commit();
        }
    }

    /**
     * Show the fragment configuring a beacon.
     */
    private void showBeaconConfigFragment() {
        BeaconConfigFragment beaconConfigFragment = BeaconConfigFragment.newInstance();
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in_and_slide_up_fragment, R.anim.fade_out_fragment, R.anim.fade_in_activity, R.anim.fade_out_fragment)
                .replace(R.id.main_activity_container, beaconConfigFragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Show the fragment displaying information about this application.
     */
    private void showAboutFragment() {
        AboutFragment aboutFragment = AboutFragment.newInstance();
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in_and_slide_up_fragment, R.anim.fade_out_fragment, R.anim.fade_in_activity, R.anim.fade_out_fragment)
                .replace(R.id.main_activity_container, aboutFragment)
                .addToBackStack(null)
                .commit();
    }

    private boolean checkIfUserHasOptedIn() {
        SharedPreferences sharedPreferences = getSharedPreferences("panda_preferences", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(getString(R.string.user_opted_in_flag), false);
    }

    /** Open another app.
     * @param context current Context, like Activity, App, or Service
     * @param packageName the full package name of the app to open
     * @return true if likely successful, false if unsuccessful
     */
    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
                //throw new PackageManager.NameNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
